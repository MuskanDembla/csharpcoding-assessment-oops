﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment2
{
    internal class OnContract : Developer
    {
        float duration;
        float chargesPerDay;

        public OnContract() : base()
        {

        }
        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter the Duration of Contract in days");
            duration = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter the Charges per Day:");
            chargesPerDay = float.Parse(Console.ReadLine());
            CalculateNetSalary();
        }
        public void CalculateNetSalary()
        {
            NetSalary = chargesPerDay * duration;
            DisplayDetails();
        }
        public void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine("The contract Duration is: {0}  days ", duration);
            Console.WriteLine("The charges perDay is: {0}", chargesPerDay);
            Console.WriteLine("Total salary is: " + NetSalary);

        }


    }
}
