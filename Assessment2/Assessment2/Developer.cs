﻿namespace Assessment2
{
    internal class Developer
    {
        public int ID;
        public string Name;
        string ProjectAssigned;
        DateTime JoiningDate;
        public float NetSalary;


        public virtual void GetDetails()
        {
            Console.WriteLine("Enter your id:");
            ID = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter your name:");
            Name = Console.ReadLine();
            Console.WriteLine("Enter joining date:");
            JoiningDate = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Name of Project assigned:");
            ProjectAssigned = Console.ReadLine();
            

        }
        public void DisplayDetails()
        {

            Console.WriteLine("ID is: " + ID);
            Console.WriteLine("Name is: " + Name);
            Console.WriteLine("Date of Joining: " + JoiningDate);
            Console.WriteLine("Project assigned: " + ProjectAssigned);

        }
    }

}
