﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Developer> list = new List<Developer>();
            int ch;
            int ch2;
            int devID;
            Console.WriteLine("-----MENU-----");
            Console.WriteLine("Enter Choice:\n 1:Create Developer\n2:Display Details");
            ch =int.Parse(Console.ReadLine());
            if (ch == 1)
            {
                Console.WriteLine("Type of Developer:\n1:On Contract\n2:On Payroll");
                ch2 = int.Parse(Console.ReadLine());
                if (ch2 == 1)
                {
                    Developer dev = new OnContract();
                   dev.GetDetails();
                    list.Add(dev);
                    ;
                }
                else if (ch2 == 2)
                {
                    Developer dev = new OnPayroll();
                    dev.GetDetails();
                    list.Add(dev);

                }
                else
                {
                    Console.WriteLine("Input is Wrong");
                }
            }
            else if (ch == 2)
            {
                Console.WriteLine("Enter Developer ID:");
                devID = int.Parse(Console.ReadLine());
                var developer = from devp in list where devp.ID == devID select devp;
                foreach (Developer dev in developer)
                {
                    dev.DisplayDetails();
                }
            }
            else
            {
                Console.WriteLine("Wrong input");
            }

            

            
            var developers = from dev in list select dev;
            foreach (Developer dev in developers)
            {
                dev.DisplayDetails();
            }

            
            var netSalary = from dev in list where dev.NetSalary > 20000 select dev;
            foreach (Developer dev in netSalary)
            {
                dev.DisplayDetails();
            }

            
            var nameContainsD = from dev in list where dev.Name.Contains('D') select dev;
            foreach (Developer dev in nameContainsD)
            {
                dev.DisplayDetails();
            }


        }
    }
}


