﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assessment2
{
    internal class OnPayroll: Developer
    {
        string dept;
        string manager;
        float baseSalary;
        float exp;
        float da;
        float hra;
        float pf;


        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter your department:");
            dept = (Console.ReadLine());
            Console.WriteLine("Enter Manager Name:");
            manager = (Console.ReadLine());
            Console.WriteLine("Enter your base salary:");
            baseSalary = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter your Experience:");
            exp = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter da:");
            da = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter hra:");
            hra = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter pf: ");
            pf = float.Parse(Console.ReadLine());
            CalculateSalary();

        }
        public void CalculateSalary()
        {
            NetSalary = baseSalary + da + hra - pf;

        }

        public void DisplayDetails()
        {
            base.DisplayDetails();
            Console.WriteLine("Department: " + dept);
            Console.WriteLine("Manager: " + manager);
            Console.WriteLine("Experience: " + exp);
            Console.WriteLine("da: " + da);
            Console.WriteLine("hra: " + hra);
            Console.WriteLine("pf: " + pf);
            Console.WriteLine("NetSalary: " + NetSalary);
            

        }
       



    }
}
